package com.example.javateststudy;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;  // main one

/*
* 테스트는 테스트마다 인스턴스를 실행하기 때문에 독립적이다.
* 그러나 이것도 junit5에서도 변경할 수 있다
*
* 순서 역시, 작성 순서대로 작성되진 않는다
* 의도는 단위테스트는 각각 독립적으로 실행가능해야하기 때문이다.
* 그러나 이것도 변경할 수 있다.
* */
@TestInstance(TestInstance.Lifecycle.PER_CLASS) //해당 클래스에서의 테스트는 하나의 인스턴스에서만 실행된다
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // 해당 어노테이션을 붙이고 메서드마다 @Order(n)을 붙이면 된다
class StudyTest {

    @Test
    @DisplayName("스터디 만들기 ☢")
    @Tag("fast")
    void create_new_study() {
        Study study = new Study();
        assertNotNull(study);
        System.out.println("create");
    }

    @Test
    @DisplayName("스터디 만들기2 ❤")
    @Tag("slow") //만약 느린 테스트라면 로컬에서 실행시키기 부담스러울 수 있기 때문에, Tag 기반으로 실행할 수 있다
    void create_new_study_again() {
        Study study = new Study();


        //assert문을 따로 나열하면, 두 번째 테스트를 가지 않는다. 그래서 assertAll를 쓰면 된다
        //그렇기 때문에 여러 검증 단계가 있으면 assertAll이 좋을 것 같다다
       assertAll(
                () -> assertNotNull(study),
                () -> assertEquals(StudyStatus.DRAFT
                        , study.getStatus()
                        ,"스터디를 처음 만들면 상태값이 DRAFT여야 한다"),
                () -> assertTrue(1 > 2, "테스트 1"),
                () -> assertTrue(1 > 2, "테스트 2")
        );

        System.out.println("create");
    }

    @DisplayName("스터디 반복 만들기")
    @RepeatedTest(value = 3, name = "{displayName}, {currentRepetition}/{totalRepetitions}")
    void repeat_test(RepetitionInfo repetitionInfo) {
        System.out.println("repeat test  ==> " + repetitionInfo.getCurrentRepetition() + "/"
                + repetitionInfo.getTotalRepetitions());
    }

    @DisplayName("스터디 반복 만들기2")
    @ParameterizedTest(name = "{index} {displayName} message={0}")
    @ValueSource(strings = {"날씨","정말","많이","춥당"}) //이렇게 파라미터를 배열 길이만큼 처리할 수 있다
    void repeat_test2(String msg) {
        System.out.println(msg);
    }

    @DisplayName("스터디 반복 만들기3")
    @ParameterizedTest(name = "{index} {displayName} message={0}")
    @ValueSource(strings = {"날씨","정말","많이","춥당"}) //이렇게 파라미터를 배열 길이만큼 처리할 수 있다
    @NullAndEmptySource //null과 빈값을 넣어서 2개 더 추가한다. 각각도 있다
    void repeat_test3(String msg) {
        System.out.println(msg);
    }

    @Test
    void create_timeout() {
        assertTimeout(Duration.ofMillis(100), () -> { //100ms 만큼 시간을 초과하면 테스트는 실패한다
            Thread.sleep(300);
        });
    }

    //assertJ등의 라이브러리를 활용해 좀 더 활용도를 높일 수 있다
    //https://assertj.github.io/doc/#use-code-completion
    @Test
    void assertJ_Test(){
        assertThat(100).isGreaterThan(50);
    }

    @Test
    @Disabled //테스트를 실행되지 않도록
    void create1() {
        System.out.println("create1");
    }

    //모든 테스트가 실행되기 전에 한번 실행됨
    @BeforeAll
    static void beforeAll() {
        System.out.println("before all");
    }

    //모든 테스트가 실행된 후에 한번 실행됨
    @AfterAll
    static void afterAll() {
        System.out.println("after all");
    }

//    @BeforeEach
//    static void beforeEach() {
//        System.out.println("before each");
//    }
//
//    //모든 테스트가 실행된 후에 한번 실행됨
//    @AfterEach
//    static void afterEach() {
//        System.out.println("after each");
//    }
}