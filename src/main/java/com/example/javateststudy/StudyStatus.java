package com.example.javateststudy;

public enum StudyStatus {
    DRAFT, OPENED, STARTED, ENDED
}
